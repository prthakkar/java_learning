import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.entity.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class MainClass {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");
		UserService service = ctx.getBean("UserService", UserServiceImpl.class);
		List<User> Users = service.getAllUserDetail();
		System.out.println("All User Details:");
		for (User User : Users) {
			System.out.println(User);
		}
		System.out.println();
		User User = service.getUserDetail(7);
		System.out.println("User Detail: " + User);
		ctx.close();
	}
}
