package com.dao;

import java.util.List;

import com.entity.User;

public interface UserDao {
	public User getUserDetail(int id);

	public List<User> getAllUserDetail();

	public int addUserDetail(User userDetail);

	public int updateUserDetail(User userDetail);

	public int deleteUserDetail(int id);
}
