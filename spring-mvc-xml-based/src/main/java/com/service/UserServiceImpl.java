package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dao.UserDao;
import com.entity.User;

public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	public User getUserDetail(int id) {
		return userDao.getUserDetail(id);
	}

	public List<User> getAllUserDetail() {
		return userDao.getAllUserDetail();
	}

	public int addUserDetail(User userDetail) {
		return userDao.addUserDetail(userDetail);
	}

	public int updateUserDetail(User userDetail) {
		return userDao.updateUserDetail(userDetail);
	}

	public int deleteUserDetail(int id) {
		return userDao.deleteUserDetail(id);
	}

	
}